/* tslint:disable:max-line-length */
import { TreoNavigationItem } from '@treo/components/navigation';

export const defaultNavigation: TreoNavigationItem[] = [
    {
        id      : 'starter',
        title   : 'SmartDocs',
        subtitle: 'SmartDocs One',
        type    : 'group',
        icon    : 'apps',
        children: [
            {
                id   : 'starter.example',
                title: 'Dashboard',
                type : 'basic',
                icon : 'heroicons_outline:template',
                link : '/example'
            },
            {
                id   : 'starter.dummy.1',
                title: 'My Team',
                icon : 'heroicons_outline:users',
                type : 'basic'
            },
            {
                id   : 'starter.dummy.2',
                title: 'Reports',
                type : 'basic',
                icon : 'heroicons_outline:document-report',
            }
        ]
    },
    {
        id      : 'applications',
        title   : 'Settings',
        subtitle: 'Settings and Support',
        type    : 'group',
        icon    : 'heroicons_outline:home',
        children: [
            {
                id      : 'applications.calendar',
                title   : 'Settings',
                type    : 'basic',
                icon    : 'heroicons_outline:cog'
            },
            {
                id   : 'applications.contacts',
                title: 'Help & Support',
                type : 'basic',
                icon : 'heroicons_outline:support'
            },
        ]
    }
];
export const compactNavigation: TreoNavigationItem[] = [
    {
        id      : 'starter',
        title   : 'Starter',
        type    : 'aside',
        icon    : 'apps',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    }
];
export const futuristicNavigation: TreoNavigationItem[] = [
    {
        id   : 'starter.example',
        title: 'Example component',
        type : 'basic',
        icon : 'heroicons:chart-pie',
        link : '/example'
    },
    {
        id   : 'starter.dummy.1',
        title: 'Dummy menu item #1',
        icon : 'heroicons:calendar',
        type : 'basic'
    },
    {
        id   : 'starter.dummy.2',
        title: 'Dummy menu item #1',
        icon : 'heroicons:user-group',
        type : 'basic'
    }
];
export const horizontalNavigation: TreoNavigationItem[] = [
    {
        id      : 'starter',
        title   : 'Starter',
        type    : 'group',
        icon    : 'apps',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    }
];
