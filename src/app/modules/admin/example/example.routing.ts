import { Route } from '@angular/router';
import { ExampleComponent } from 'app/modules/admin/example/example.component';
import { ExampleDetailsComponent } from './example-details/example-details.component';

export const exampleRoutes: Route[] = [
    {
        path     : '',
        children: [
            {
                path: '',
                component: ExampleComponent
            },
            {
                path: 'details',
                component: ExampleDetailsComponent
            }
        ]
    }
];
