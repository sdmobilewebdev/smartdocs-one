import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example-details',
  templateUrl: './example-details.component.html',
  styleUrls: ['./example-details.component.scss']
})
export class ExampleDetailsComponent implements OnInit {

  clickIndex: any = {
    id_one: true,
    id_two: false,
    id_three: false
  }

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(id) {
    switch (id) {
      case 1:
        this.clickIndex.id_one = true;
        this.clickIndex.id_two = false;
        this.clickIndex.id_three = false;
        break;
      case 2:
        this.clickIndex.id_one = false;
        this.clickIndex.id_two = true;
        this.clickIndex.id_three = false;
        break;
      case 3:
        this.clickIndex.id_one = false;
        this.clickIndex.id_two = false;
        this.clickIndex.id_three = true;
        break;
      default: 
        break;
    }
  }

}
