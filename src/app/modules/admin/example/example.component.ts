import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewEncapsulation } from '@angular/core';
import { constructor } from 'lodash';
import { ApexOptions } from 'ng-apexcharts';

@Component({
    selector: 'example',
    templateUrl: './example.component.html',
    styleUrls: ['./example.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExampleComponent {

    barChart: ApexOptions;
    pieChart1: ApexOptions;
    pieChart2: ApexOptions;
    isChartsLoaded: boolean = false;

    constructor(
        private cdr: ChangeDetectorRef
    ) {
        setTimeout(() => {
            this.barChart = {
                chart: {
                    type: 'bar'
                },
                series: [
                    {
                        name: 'Invoices',
                        data: [150, 186, 218]
                    }
                ],
                xaxis: {
                    categories: [2019, 2020, 2021]
                }
            };

            this.pieChart1 = {
                series: [44, 55, 13],
                chart: {
                    type: "donut"
                },
                colors: ['#088FFB', '#46AFFF', '#81C8FF'],
                labels: ["Collaborated", "Credit", "Exception"],
                dataLabels: {
                    enabled: false,
                },
                responsive: [
                    {
                        breakpoint: 480,
                        options: {
                            chart: {
                                width: 200
                            },
                            legend: {
                                position: "bottom"
                            }
                        }
                    }
                ]
            }

            this.pieChart2 = {
                series: [75, 32],
                chart: {
                    type: "donut"
                },
                colors: ['#088FFB', '#46AFFF'],
                labels: ["Collaborated", "Credit"],
                dataLabels: {
                    enabled: false,
                },
                responsive: [
                    {
                        breakpoint: 480,
                        options: {
                            chart: {
                                width: 200
                            },
                            legend: {
                                position: "bottom"
                            }
                        }
                    }
                ]
            }
            this.isChartsLoaded = true;
            this.cdr.detectChanges();
        }, 500);
    }
}
