import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ExampleComponent } from 'app/modules/admin/example/example.component';
import { exampleRoutes } from 'app/modules/admin/example/example.routing';
import { MatIconModule } from '@angular/material/icon';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatMenuModule } from '@angular/material/menu';
import { ExampleDetailsComponent } from './example-details/example-details.component';

@NgModule({
    declarations: [
        ExampleComponent,
        ExampleDetailsComponent
    ],
    imports     : [
        RouterModule.forChild(exampleRoutes),
        SharedModule,
        MatIconModule,
        NgApexchartsModule,
        MatMenuModule
    ]
})
export class ExampleModule
{
}
